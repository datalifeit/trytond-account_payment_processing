# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.tests.test_tryton import ModuleTestCase


class AccountPaymentProcessingTestCase(ModuleTestCase):
    'Test Account Payment Processing module'
    module = 'account_payment_processing'
    extras = ['account_bank_statement_payment']


del ModuleTestCase
